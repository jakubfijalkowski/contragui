﻿using System.Collections.Generic;

namespace ContraGUI.Base
{
    /// <summary>
    /// Extensions for <see cref="IList{RendererObject}"/>.
    /// </summary>
    public static class ObjectListExtensions
    {
        /// <summary>
        /// Removes an object from a list.
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="obj"></param>
        /// <param name="disposeBrush"></param>
        /// <returns></returns>
        public static bool RemoveAndDispose(this IList<IRendererObject> objects, IRendererObject obj, bool disposeBrush = true)
        {
            if (objects.Remove(obj))
            {
                obj.Dispose(disposeBrush);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes an object at specified index.
        /// </summary>
        /// <remarks>
        /// If the object derives from <see cref="RendererObject"/>, this method uses <see cref="RendererObject.Dispose(bool)"/>.
        /// If it's not, it uses plain <see cref="IDisposable.Dispose"/>.
        /// </remarks>
        /// <param name="objects"></param>
        /// <param name="index"></param>
        /// <param name="disposeBrush"></param>
        public static void RemoveAtAndDispose(this IList<IRendererObject> objects, int index, bool disposeBrush = true)
        {
            var obj = objects[index];
            objects.RemoveAt(index);
            obj.Dispose(disposeBrush);
        }

        /// <summary>
        /// Removes and disposes all objects in the list.
        /// </summary>
        /// <remarks>
        /// If an object derives from <see cref="RendererObject"/>, this method uses <see cref="RendererObject.Dispose(bool)"/>.
        /// If it's not, it uses plain <see cref="IDisposable.Dispose"/>.
        /// </remarks>
        /// <param name="objects"></param>
        /// <param name="disposeBrush"></param>
        public static void ClearAndDispose(this IList<IRendererObject> objects, bool disposeBrush = true)
        {
            foreach (var item in objects)
            {
                item.Dispose(disposeBrush);
            }
            objects.Clear();
        }
    }
}
