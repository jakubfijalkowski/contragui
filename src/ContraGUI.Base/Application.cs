﻿using System;
using System.Diagnostics;
using SharpDX.Direct3D10;
using SharpDX.DXGI;
using SharpDX.Windows;

namespace ContraGUI.Base
{
    using Device1 = SharpDX.Direct3D10.Device1;
    using DriverType = SharpDX.Direct3D10.DriverType;
    using FeatureLevel = SharpDX.Direct3D10.FeatureLevel;

    /// <summary>
    /// Entry point for your libraries.
    /// </summary>
    public abstract class Application
    {
        private Window mainWindow;
        private Device1 device;
        private SwapChain swapChain;

        private Renderer renderer;

        private readonly Stopwatch timer = new Stopwatch();
        private TimeSpan lastTime = default(TimeSpan);

        /// <summary>
        /// Gets the main window where everything happens. ;)
        /// </summary>
        public IWindow MainWindow
        {
            get { return this.mainWindow; }
        }

        /// <summary>
        /// Gets a renderer associated with this application.
        /// </summary>
        public IRenderer Renderer
        {
            get { return this.renderer; }
        }

        /// <summary>
        /// This is the place where you should put your initialization code, eg. wiring up events.
        /// </summary>
        protected virtual void OnInitialize()
        {

        }

        /// <summary>
        /// Here you should put the code that does cleanup, eg. frees resources.
        /// </summary>
        protected virtual void OnDeinitialize()
        {

        }

        /// <summary>
        /// Here, you can modify your renderer logic - ie. make statefull renderer a stateless one.
        /// </summary>
        /// <param name="elapsedTime"></param>
        protected virtual void OnRender(TimeSpan elapsedTime)
        {
            this.renderer.Render();
        }

        /// <summary>
        /// Runs the application, corresponding message pump and initializes the process of rendering the window.
        /// </summary>
        public void Run()
        {
            this.Initialize();
            this.timer.Start();
            RenderLoop.Run(this.mainWindow, this.Render);
            this.timer.Stop();
            this.Deinitialize();
        }

        private void Initialize()
        {
            this.mainWindow = new Window();
            this.mainWindow.UserResized += OnWindowResized;

            var swapChainDesc = new SwapChainDescription()
            {
                BufferCount = 1,
                ModeDescription = new ModeDescription(this.mainWindow.ClientSize.Width, this.mainWindow.ClientSize.Height, new Rational(60, 1), Format.R8G8B8A8_UNorm),
                IsWindowed = true,
                OutputHandle = this.mainWindow.Handle,
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };
            Device1.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.BgraSupport, swapChainDesc, FeatureLevel.Level_10_0, out this.device, out this.swapChain);

            using (var factory = swapChain.GetParent<Factory>())
            {
                factory.MakeWindowAssociation(this.mainWindow.Handle, WindowAssociationFlags.IgnoreAll);
            }

            this.renderer = new Renderer(this.device, this.swapChain);
            this.renderer.AcquireRenderTarget();

            this.OnInitialize();
        }

        private void Deinitialize()
        {
            this.OnDeinitialize();

            this.renderer.Dispose();
            this.swapChain.Dispose();
            this.device.ClearState();
            this.device.Flush();
            this.device.Dispose();
        }

        private void OnWindowResized(object sender, System.EventArgs e)
        {
            this.renderer.ReleaseRenderTarget();
            this.swapChain.ResizeBuffers(0, 0, 0, Format.Unknown, SwapChainFlags.None);
            this.renderer.AcquireRenderTarget();
        }

        private void Render()
        {
            var time = this.timer.Elapsed;
            this.OnRender(time - this.lastTime);
            this.lastTime = time;
        }
    }
}
