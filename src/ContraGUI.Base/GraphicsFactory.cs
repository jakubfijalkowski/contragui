﻿using System;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DirectWrite;

namespace ContraGUI.Base
{
    using Factory = SharpDX.Direct2D1.Factory;
    using TextFactory = SharpDX.DirectWrite.Factory;

    sealed class GraphicsFactory
        : IGraphicsFactory, IDisposable
    {
        private readonly Factory factory;
        private readonly TextFactory textFactory;

        public RenderTarget RenderTarget { get; set; }

        public GraphicsFactory(Factory factory)
        {
            this.factory = factory;
            this.textFactory = new TextFactory(SharpDX.DirectWrite.FactoryType.Shared);
        }

        public SolidColorBrush CreateSolidColorBrush(Color4 color)
        {
            return new SolidColorBrush(this.RenderTarget, color);
        }

        public LinearGradientBrush CreateLinearGradientBrush(LinearGradientBrushProperties properties, params GradientStop[] gradientStops)
        {
            using (var stopCollection = new GradientStopCollection(this.RenderTarget, gradientStops))
            {
                return new LinearGradientBrush(this.RenderTarget, properties, stopCollection);
            }
        }

        public RadialGradientBrush CreateRadialGradientBrush(RadialGradientBrushProperties properties, params GradientStop[] gradientStops)
        {
            using (var stopCollection = new GradientStopCollection(this.RenderTarget, gradientStops))
            {
                return new RadialGradientBrush(this.RenderTarget, properties, stopCollection);
            }
        }

        public FilledObject FillRectangle(RectangleF rect, Brush brush)
        {
            return new FilledObject(new RectangleGeometry(this.factory, rect), brush);
        }

        public StrokedObject DrawRectangle(RectangleF rect, float strokeWidth, Brush brush)
        {
            return new StrokedObject(new RectangleGeometry(this.factory, rect), brush, strokeWidth);
        }

        public FilledObject FillRoundedRectangle(RectangleF rect, float radiusX, float radiusY, Brush brush)
        {
            return new FilledObject(
                new RoundedRectangleGeometry(this.factory, new RoundedRectangle
                {
                    RadiusX = radiusX,
                    RadiusY = radiusY,
                    Rect = rect
                }), brush);
        }

        public StrokedObject DrawRoundedRectangle(RectangleF rect, float radiusX, float radiusY, float strokeWidth, Brush brush)
        {
            return new StrokedObject(
                new RoundedRectangleGeometry(this.factory, new RoundedRectangle
                {
                    RadiusX = radiusX,
                    RadiusY = radiusY,
                    Rect = rect
                }), brush, strokeWidth);
        }

        public FilledObject FillEllipse(Vector2 center, float radiusX, float radiusY, Brush brush)
        {
            return new FilledObject(new EllipseGeometry(this.factory, new Ellipse(center, radiusX, radiusY)), brush);
        }

        public StrokedObject DrawEllipse(Vector2 center, float radiusX, float radiusY, float strokeWidth, Brush brush)
        {
            return new StrokedObject(new EllipseGeometry(this.factory, new Ellipse(center, radiusX, radiusY)), brush, strokeWidth);
        }

        public StrokedObject DrawLine(Vector2 start, Vector2 end, float width, Brush brush)
        {
            var geometry = new PathGeometry(this.factory);
            using (var sink = geometry.Open())
            {
                sink.BeginFigure(start, FigureBegin.Hollow);
                sink.AddLine(end);
                sink.EndFigure(FigureEnd.Open);
                sink.Close();
            }

            return new StrokedObject(geometry, brush, width);
        }

        public FilledObject FillPath(Brush brush)
        {
            var geometry = new PathGeometry(this.factory);
            return new FilledObject(geometry, brush);
        }

        public StrokedObject DrawPath(float strokeWidth, Brush brush)
        {
            var geometry = new PathGeometry(this.factory);
            return new StrokedObject(geometry, brush, strokeWidth);
        }

        public TextObject DrawText(Vector2 position, string text, string fontName, FontWeight fontWeight, FontStyle fontStyle, float fontSize, float maxWidth, float maxHeight, Brush brush)
        {
            var format = new TextFormat(this.textFactory, fontName, fontWeight, fontStyle, fontSize);
            var layout = new TextLayout(this.textFactory, text, format, maxWidth, maxHeight);
            return new TextObject(position, format, layout, brush);
        }

        public void Dispose()
        {
            this.textFactory.Dispose();
        }
    }
}
