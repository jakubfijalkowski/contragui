﻿using System;
using System.Collections.Generic;

namespace ContraGUI.Base
{
    sealed class ObjectList
        : List<IRendererObject>, IDisposable
    {
        public void Dispose()
        {
            foreach (var item in this)
            {
                item.Dispose(true);
            }
            this.Clear();
        }
    }
}
