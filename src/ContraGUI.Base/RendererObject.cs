﻿using System;
using SharpDX.Direct2D1;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents a real object(single, not grouped) that may be drawn on the screen using
    /// particular <see cref="Brush"/>, eg. rectangle.
    /// </summary>
    public abstract class RendererObject
        : IRendererObject
    {
        private Brush brush;

        /// <summary>
        /// Gets a brush used to paint the area.
        /// </summary>
        /// <remarks>
        /// If you need to manipulate the properties of the brush, feel free to cast it to concrete type.
        /// </remarks>
        public Brush Brush
        {
            get { return this.brush; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                this.brush = value;
            }
        }

        internal RendererObject()
        { }

        /// <summary>
        /// Disposes the whole object(both <see cref="Geometry"/> and <see cref="Brush"/>).
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Disposes the object. Allows to decide if <see cref="Brush"/> should be disposed.
        /// </summary>
        /// <param name="disposeBrush"></param>
        public abstract void Dispose(bool disposeBrush);

        /// <inheritdoc />
        public abstract void Render(RenderTarget renderTarget);
    }
}
