﻿using System.Collections.Generic;

namespace ContraGUI.Base
{
    /// <summary>
    /// Renderer that allows drawing basic primitives.
    /// Stateful, so everything is preserved between frames and you have to manually delete
    /// object from the renderer to make it disappear.
    /// </summary>
    public interface IRenderer
    {
        /// <summary>
        /// Gets the list of object currently being drawn by the renderer.
        /// </summary>
        IList<IRendererObject> Objects { get; }

        /// <summary>
        /// Gets the factory of graphics.
        /// </summary>
        IGraphicsFactory Factory { get; }

        /// <summary>
        /// Renders objects from <see cref="Objects"/> on the screen.
        /// </summary>
        void Render();
    }
}
