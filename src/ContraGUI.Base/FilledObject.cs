﻿using SharpDX.Direct2D1;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents a primitive object that is being drawn as filled.
    /// </summary>
    public sealed class FilledObject
        : RendererObject
    {
        /// <summary>
        /// Gets a geometry of the object.
        /// </summary>
        /// <remarks>
        /// If you need to manipulate the geometry, feel free to cast it to concrete type.
        /// </remarks>
        public Geometry Geometry { get; private set; }

        internal FilledObject(Geometry geometry, Brush brush)
        {
            this.Geometry = geometry;
            this.Brush = brush;
        }

        /// <inheritdoc />
        public override void Dispose(bool disposeBrush)
        {
            this.Geometry.Dispose();
            if (disposeBrush)
            {
                this.Brush.Dispose();
            }
        }

        /// <inheritdoc />
        public override void Render(RenderTarget renderTarget)
        {
            renderTarget.FillGeometry(this.Geometry, this.Brush);
        }
    }
}
