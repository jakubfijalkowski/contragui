﻿using System.Collections.Generic;
using SharpDX.Direct2D1;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents a group of objects that may be renderer using single <see cref="Render"/> call.
    /// May be useful for layering objects.
    /// </summary>
    public sealed class ObjectsGroup
        : List<IRendererObject>, IRendererObject
    {
        /// <inheritdoc />
        public void Render(RenderTarget renderTarget)
        {
            foreach (var item in this)
            {
                item.Render(renderTarget);
            }
        }

        /// <inheritdoc />
        public void Dispose(bool disposeBrush)
        {
            this.ClearAndDispose(disposeBrush);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            foreach (var item in this)
            {
                item.Dispose();
            }
            this.Clear();
        }
    }
}
