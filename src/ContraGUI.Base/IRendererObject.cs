﻿using System;
using SharpDX.Direct2D1;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents an object that contains logic for drawing itself onto a render target.
    /// </summary>
    public interface IRendererObject
        : IDisposable
    {
        /// <summary>
        /// Renders the object.
        /// </summary>
        /// <param name="renderTarget"></param>
        void Render(RenderTarget renderTarget);

        /// <summary>
        /// Disposes the whole object - if <see cref="Brush"/> is present, it will be disposed too.
        /// </summary>
        /// <param name="disposeBrush"></param>
        void Dispose(bool disposeBrush);
    }
}
