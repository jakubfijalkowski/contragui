﻿using System;
using SharpDX;

namespace ContraGUI.Sample
{
    sealed class Program
        : GUIApplication
    {
        private Label lbl;
        private Button btn;

        private int clicks = 0;

        static void Main(string[] args)
        {
            new Program().Run();
        }

        protected override Control BuildGUI()
        {
            var layout = new AbsoluteLayout();

            this.lbl = new Label(new Vector2(10, 10), new Size2F(100, 20), "Simple label");
            this.btn = new Button(new Vector2(10, 50), new Size2F(100, 50), "Button!");

            this.btn.Click += this.OnButtonClick;

            layout.Add(this.lbl);
            layout.Add(this.btn);

            return layout;
        }

        private void OnButtonClick(object sender, EventArgs e)
        {
            this.clicks++;
            this.lbl.Text = "Clicks: " + this.clicks;
        }
    }
}
