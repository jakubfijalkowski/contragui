﻿using System;
using ContraGUI.Base;

namespace ContraGUI.Sample
{
    sealed class GUIContainer
        : Control
    {
        private Control root;

        public GUIContainer(IWindow wnd, IRenderer renderer)
        {
            wnd.MouseMove += this.OnMouseMove;
            wnd.MouseDown += this.OnMouseDown;
            wnd.MouseUp += this.OnMouseUp;

            renderer.Objects.Add(this.Visuals);
        }

        public void SetRoot(Control root)
        {
            this.root = root;
            this.Visuals.Add(root.Visuals);
        }

        protected override void OnParentChanged(Control oldParent, Control newParent)
        {
            throw new InvalidOperationException("Changing Parent of root object is not supported.");
        }

        private void OnMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.root.OnMouseMove(e);
        }

        private void OnMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.root.OnMouseDown(e);
        }

        private void OnMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.root.OnMouseUp(e);
        }
    }
}
