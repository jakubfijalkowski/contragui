﻿using ContraGUI.Base;
using SharpDX;
using SharpDX.DirectWrite;

namespace ContraGUI.Sample
{
    public class Label
        : Control
    {
        private Vector2 position;
        private Size2F size;
        private string text;

        private string fontName = "Segoe UI";
        private float fontSize = 12;
        private Color4 foregroundColor = Color.Black;
        private Color4 backgroundColor = Color.Transparent;

        public Vector2 Position
        {
            get { return this.position; }
            set
            {
                if (this.position != value)
                {
                    this.position = value;
                    this.OnRender();
                }
            }
        }

        public Size2F Size
        {
            get { return this.size; }
            set
            {
                if (this.size != value)
                {
                    this.size = value;
                    this.OnRender();
                }
            }
        }

        public string Text
        {
            get { return this.text; }
            set
            {
                if (this.text != value)
                {
                    this.text = value;
                    this.OnRender();
                }
            }
        }

        public string FontName
        {
            get { return this.fontName; }
            set
            {
                if (this.fontName != value)
                {
                    this.fontName = value;
                    this.OnRender();
                }
            }
        }

        public float FontSize
        {
            get { return this.fontSize; }
            set
            {
                if (this.fontSize != value)
                {
                    this.fontSize = value;
                    this.OnRender();
                }
            }
        }

        public Color4 ForegroundColor
        {
            get { return this.foregroundColor; }
            set
            {
                if (this.foregroundColor != value)
                {
                    this.foregroundColor = value;
                    this.OnRender();
                }
            }
        }

        public Color4 BackgroundColor
        {
            get { return this.backgroundColor; }
            set
            {
                if (this.backgroundColor != value)
                {
                    this.backgroundColor = value;
                    this.OnRender();
                }
            }
        }

        public Label(Vector2 pos, Size2F size, string text)
        {
            this.position = pos;
            this.size = size;
            this.text = text;
            this.OnRender();
        }

        protected virtual void OnRender()
        {
            this.Visuals.ClearAndDispose();

            var bgBrush = Factory.CreateSolidColorBrush(this.BackgroundColor);
            var fgBrush = Factory.CreateSolidColorBrush(this.ForegroundColor);
            var bg = Factory.FillRectangle(new RectangleF(this.Position.X, this.position.Y, this.Size.Width, this.Size.Height), bgBrush);
            var text = Factory.DrawText(
                this.Position, this.Text,
                this.fontName, FontWeight.Normal, FontStyle.Normal,
                this.FontSize, this.Size.Width, this.Size.Height,
                fgBrush);

            this.Visuals.Add(bg);
            this.Visuals.Add(text);
        }
    }
}
