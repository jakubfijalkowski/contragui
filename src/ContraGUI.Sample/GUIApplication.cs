﻿using System;
using ContraGUI.Base;

namespace ContraGUI.Sample
{
    public abstract class GUIApplication
        : Application
    {
        /// <remarks>
        /// Warning - this is NOT thread safe in any manner.
        /// </remarks>
        public static GUIApplication Current { get; private set; }

        private GUIContainer container;

        public GUIApplication()
        {
            if (Current != null)
            {
                throw new InvalidOperationException("There may be only one GUIApplication at a time.");
            }
            Current = this;
        }

        public IGraphicsFactory Factory
        {
            get { return this.Renderer.Factory; }
        }

        protected sealed override void OnInitialize()
        {
            this.MainWindow.Text = "GUI application";

            this.container = new GUIContainer(this.MainWindow, this.Renderer);

            var root = this.BuildGUI();
            this.container.SetRoot(root);
        }

        protected abstract Control BuildGUI();
    }
}
