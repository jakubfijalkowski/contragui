﻿using System.Windows.Forms;
using SharpDX;

namespace ContraGUI.Sample
{
    static class MouseEventExtensions
    {
        public static bool IsOver(this MouseEventArgs e, Vector2 pos, Size2F size)
        {
            var p = e.Location;
            return p.X >= pos.X && p.Y >= pos.Y && p.X <= pos.X + size.Width && p.Y <= pos.Y + size.Height;
        }
    }
}
