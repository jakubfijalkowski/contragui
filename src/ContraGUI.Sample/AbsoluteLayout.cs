﻿using System.Collections.Generic;

namespace ContraGUI.Sample
{
    public class AbsoluteLayout
        : Control, IList<Control>
    {
        private readonly List<Control> controls = new List<Control>();

        public int Count
        {
            get { return this.controls.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public Control this[int index]
        {
            get { return this.controls[index]; }
            set
            {
                this.RemoveAt(index);
                this.Insert(index, value);
            }
        }

        public void Add(Control item)
        {
            this.Insert(this.controls.Count, item);
        }

        public void Insert(int index, Control item)
        {
            this.controls.Insert(index, item);
            this.Visuals.Insert(index, item.Visuals);
            item.Parent = this;
        }

        public bool Remove(Control item)
        {
            if (this.controls.Remove(item))
            {
                item.Parent = null;
                this.Visuals.Remove(item.Visuals);
                return true;
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            var obj = this.controls[index];
            this.controls.RemoveAt(index);
            this.Visuals.RemoveAt(index);
            obj.Parent = null;
        }

        public void Clear()
        {
            foreach (var item in this.controls)
            {
                item.Parent = null;
            }
            this.controls.Clear();
            this.Visuals.Clear();
        }

        public int IndexOf(Control item)
        {
            return this.controls.IndexOf(item);
        }

        public bool Contains(Control item)
        {
            return this.controls.Contains(item);
        }

        public void CopyTo(Control[] array, int arrayIndex)
        {
            this.controls.CopyTo(array, arrayIndex);
        }

        public IEnumerator<Control> GetEnumerator()
        {
            return this.controls.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.controls.GetEnumerator();
        }

        public override bool OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            foreach (var item in this.controls)
            {
                if (item.OnMouseMove(e))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            foreach (var item in this.controls)
            {
                if (item.OnMouseDown(e))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            foreach (var item in this.controls)
            {
                if (item.OnMouseUp(e))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
