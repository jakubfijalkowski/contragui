﻿using System;
using System.Windows.Forms;
using ContraGUI.Base;
using SharpDX;
using SharpDX.DirectWrite;

namespace ContraGUI.Sample
{
    public enum ButtonState
    {
        Normal,
        Hover,
        Pressed
    }

    public sealed class ButtonStateChangedEventArgs
        : EventArgs
    {
        public ButtonState OldState { get; private set; }

        public ButtonState NewState { get; private set; }

        public ButtonStateChangedEventArgs(ButtonState oldState, ButtonState newState)
        {
            this.OldState = oldState;
            this.NewState = newState;
        }
    }


    public class Button
        : Control
    {
        const float BorderOffset = 5.0f;
        const float BorderSize = 2.0f;

        private Vector2 position;
        private Size2F size;
        private string text;

        private Color4 normalBackground = new Color4(0xFFCCCCCC);
        private Color4 hoverBackground = new Color4(0xFFE0E0E0);
        private Color4 pressedBackground = new Color4(0xFFF7F7F7);
        private Color4 borderColor = Color.Black;
        private Color4 foregroundColor = Color.Black;

        private string fontName = "Segoe UI";
        private float fontSize = 12;

        private ButtonState state;

        public Vector2 Position
        {
            get { return this.position; }
            set
            {
                if (this.position != value)
                {
                    this.position = value;
                    this.OnRender();
                }
            }
        }

        public Size2F Size
        {
            get { return this.size; }
            set
            {
                if (this.size != value)
                {
                    this.size = value;
                    this.OnRender();
                }
            }
        }

        public string Text
        {
            get { return this.text; }
            set
            {
                if (this.text != value)
                {
                    this.text = value;
                    this.OnRender();
                }
            }
        }

        public Color4 NormalBackground
        {
            get { return this.normalBackground; }
            set
            {
                if (this.normalBackground != value)
                {
                    this.normalBackground = value;
                    this.OnRender();
                }
            }
        }

        public Color4 HoverBackground
        {
            get { return this.hoverBackground; }
            set
            {
                if (this.hoverBackground != value)
                {
                    this.hoverBackground = value;
                    this.OnRender();
                }
            }
        }

        public Color4 PressedBackground
        {
            get { return this.pressedBackground; }
            set
            {
                if (this.pressedBackground != value)
                {
                    this.pressedBackground = value;
                    this.OnRender();
                }
            }
        }

        public Color4 BorderColor
        {
            get { return this.borderColor; }
            set
            {
                if (this.borderColor != value)
                {
                    this.borderColor = value;
                    this.OnRender();
                }
            }
        }

        public Color4 ForegroundColor
        {
            get { return this.foregroundColor; }
            set
            {
                if (this.foregroundColor != value)
                {
                    this.foregroundColor = value;
                    this.OnRender();
                }
            }
        }

        public string FontName
        {
            get { return this.fontName; }
            set
            {
                if (this.fontName != value)
                {
                    this.fontName = value;
                    this.OnRender();
                }
            }
        }

        public float FontSize
        {
            get { return this.fontSize; }
            set
            {
                if (this.fontSize != value)
                {
                    this.fontSize = value;
                    this.OnRender();
                }
            }
        }

        public ButtonState State
        {
            get { return this.state; }
            private set
            {
                if (this.state != value)
                {
                    var old = this.state;
                    this.state = value;
                    this.OnStateChanged(old, this.state);
                }
            }
        }

        public event EventHandler<ButtonStateChangedEventArgs> StateChanged;
        public event EventHandler Click;

        public Button(Vector2 pos, Size2F size, string text)
        {
            this.position = pos;
            this.size = size;
            this.text = text;

            this.OnRender();
        }

        protected virtual void OnRender()
        {
            this.Visuals.ClearAndDispose();

            Color4 bgColor = this.State == ButtonState.Normal ? this.NormalBackground :
                this.State == ButtonState.Hover ? this.HoverBackground :
                this.PressedBackground;

            var bgBrush = Factory.CreateSolidColorBrush(bgColor);
            var bg = Factory.FillRectangle(new RectangleF(this.Position.X, this.Position.Y, this.Size.Width, this.Size.Height), bgBrush);


            var contentRect = new RectangleF(
                this.Position.X + BorderOffset, this.Position.Y + BorderOffset,
                this.Size.Width - BorderOffset * 2, this.Size.Height - BorderOffset * 2);

            var borderBrush = Factory.CreateSolidColorBrush(this.BorderColor);
            var border = Factory.DrawRectangle(contentRect, BorderSize, borderBrush);

            var fgBrush = Factory.CreateSolidColorBrush(this.ForegroundColor);
            var text = Factory.DrawText(
                this.Position + BorderOffset, this.Text,
                this.fontName, FontWeight.Normal, FontStyle.Normal,
                this.FontSize, this.Size.Width - BorderOffset, this.Size.Height - BorderOffset,
                fgBrush);

            var x = (contentRect.Width - text.Layout.Metrics.Width) / 2;
            var y = (contentRect.Height - text.Layout.Metrics.Height) / 2;
            text.Position += new Vector2(x, y);

            this.Visuals.Add(bg);
            this.Visuals.Add(border);
            this.Visuals.Add(text);
        }

        private void OnStateChanged(ButtonState oldState, ButtonState newState)
        {
            this.OnRender();

            var @event = this.StateChanged;
            if (@event != null)
            {
                @event(this, new ButtonStateChangedEventArgs(oldState, newState));
            }
            if (newState == ButtonState.Pressed)
            {
                this.OnClick();
            }
        }

        private void OnClick()
        {
            var @event = this.Click;
            if (@event != null)
            {
                @event(this, EventArgs.Empty);
            }
        }

        public override bool OnMouseMove(MouseEventArgs e)
        {
            if (e.IsOver(this.Position, this.Size))
            {
                if (this.State == ButtonState.Normal)
                {
                    this.State = ButtonState.Hover;
                }
                return true;
            }
            else
            {
                this.State = ButtonState.Normal;
            }
            return false;
        }

        public override bool OnMouseDown(MouseEventArgs e)
        {
            if (e.IsOver(this.Position, this.Size))
            {
                this.State = ButtonState.Pressed;
                return true;
            }
            return false;
        }

        public override bool OnMouseUp(MouseEventArgs e)
        {
            if (e.IsOver(this.Position, this.Size))
            {
                this.State = ButtonState.Hover;
                return true;
            }
            this.State = ButtonState.Normal;
            return false;
        }
    }
}
