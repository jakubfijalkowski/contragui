﻿using System;
using System.Windows.Forms;
using ContraGUI.Base;

namespace ContraGUI.Sample
{
    public abstract class Control
        : IDisposable
    {
        private readonly ObjectsGroup visuals = new ObjectsGroup();
        private Control parent;

        private bool disposed = false;

        public ObjectsGroup Visuals
        {
            get { return this.visuals; }
        }

        public Control Parent
        {
            get { return this.parent; }
            set
            {
                if (this.parent != value)
                {
                    var old = this.parent;
                    this.parent = value;
                    this.OnParentChanged(old, this.parent);
                }
            }
        }

        protected static IGraphicsFactory Factory
        {
            get { return GUIApplication.Current.Factory; }
        }

        protected virtual void OnParentChanged(Control oldParent, Control newParent)
        { }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.Visuals.Dispose(true);
            }

            this.disposed = true;
        }

        public virtual bool OnMouseMove(MouseEventArgs e)
        {
            return false;
        }

        public virtual bool OnMouseDown(MouseEventArgs e)
        {
            return false;
        }

        public virtual bool OnMouseUp(MouseEventArgs e)
        {
            return false;
        }
    }
}
